﻿namespace DevPac_Creator
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtDeveloper = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRequestor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEnvironment = new System.Windows.Forms.TextBox();
            this.chkSecurity = new System.Windows.Forms.CheckBox();
            this.txtBATester = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnChangeDate = new System.Windows.Forms.Button();
            this.txtChangeDate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPurpose = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkWEB = new System.Windows.Forms.CheckBox();
            this.chkPORTAL = new System.Windows.Forms.CheckBox();
            this.chkFIN = new System.Windows.Forms.CheckBox();
            this.chkCRM = new System.Windows.Forms.CheckBox();
            this.chkCS = new System.Windows.Forms.CheckBox();
            this.txtTicketNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnChangeRequest = new System.Windows.Forms.Button();
            this.txtChangeRequestNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.optExecuteBuild = new System.Windows.Forms.RadioButton();
            this.optExecuteNow = new System.Windows.Forms.RadioButton();
            this.optBuildScript = new System.Windows.Forms.RadioButton();
            this.txtBuildRequired = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkCreateTrigger = new System.Windows.Forms.CheckBox();
            this.chkAlterTable = new System.Windows.Forms.CheckBox();
            this.chkCreateViews = new System.Windows.Forms.CheckBox();
            this.chkCreateIndex = new System.Windows.Forms.CheckBox();
            this.chkCreateTable = new System.Windows.Forms.CheckBox();
            this.chkBuildRequired = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmboDestination = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmboSource = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmboPillar = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtAlterAny = new System.Windows.Forms.TextBox();
            this.chkChangesToDefault = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.chkAlterDeletes = new System.Windows.Forms.CheckBox();
            this.chkAlterRenames = new System.Windows.Forms.CheckBox();
            this.chkAlterChanges = new System.Windows.Forms.CheckBox();
            this.chkAlterAdd = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.btnNext = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabs.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabPage1);
            this.tabs.Controls.Add(this.tabPage3);
            this.tabs.Controls.Add(this.tabPage5);
            this.tabs.Controls.Add(this.tabPage2);
            this.tabs.Location = new System.Drawing.Point(12, 26);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(462, 381);
            this.tabs.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox4);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.txtDeveloper);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtRequestor);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtEnvironment);
            this.tabPage1.Controls.Add(this.chkSecurity);
            this.tabPage1.Controls.Add(this.txtBATester);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.btnChangeDate);
            this.tabPage1.Controls.Add(this.txtChangeDate);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtPurpose);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtDescription);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.chkWEB);
            this.tabPage1.Controls.Add(this.chkPORTAL);
            this.tabPage1.Controls.Add(this.chkFIN);
            this.tabPage1.Controls.Add(this.chkCRM);
            this.tabPage1.Controls.Add(this.chkCS);
            this.tabPage1.Controls.Add(this.txtTicketNumber);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btnChangeRequest);
            this.tabPage1.Controls.Add(this.txtChangeRequestNumber);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(454, 355);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General Info";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtDeveloper
            // 
            this.txtDeveloper.Location = new System.Drawing.Point(132, 213);
            this.txtDeveloper.Name = "txtDeveloper";
            this.txtDeveloper.Size = new System.Drawing.Size(219, 20);
            this.txtDeveloper.TabIndex = 13;
            this.txtDeveloper.Tag = "<! <DEVELOPER>!>";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(67, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 22;
            this.label8.Tag = "";
            this.label8.Text = "Developer:";
            // 
            // txtRequestor
            // 
            this.txtRequestor.Location = new System.Drawing.Point(132, 161);
            this.txtRequestor.Name = "txtRequestor";
            this.txtRequestor.Size = new System.Drawing.Size(250, 20);
            this.txtRequestor.TabIndex = 10;
            this.txtRequestor.Tag = "<! <REQUESTOR>!>";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(67, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Requestor:";
            // 
            // txtEnvironment
            // 
            this.txtEnvironment.Location = new System.Drawing.Point(327, 15);
            this.txtEnvironment.Name = "txtEnvironment";
            this.txtEnvironment.Size = new System.Drawing.Size(94, 20);
            this.txtEnvironment.TabIndex = 18;
            this.txtEnvironment.Tag = "<! <ENVIRONMENT>!>";
            this.txtEnvironment.Visible = false;
            // 
            // chkSecurity
            // 
            this.chkSecurity.AutoSize = true;
            this.chkSecurity.Location = new System.Drawing.Point(132, 308);
            this.chkSecurity.Name = "chkSecurity";
            this.chkSecurity.Size = new System.Drawing.Size(110, 17);
            this.chkSecurity.TabIndex = 15;
            this.chkSecurity.Tag = "<! <SECURITY>!>";
            this.chkSecurity.Text = "Security Required";
            this.chkSecurity.UseVisualStyleBackColor = true;
            this.chkSecurity.CheckedChanged += new System.EventHandler(this.chkSecurity_CheckedChanged);
            // 
            // txtBATester
            // 
            this.txtBATester.Location = new System.Drawing.Point(132, 239);
            this.txtBATester.Name = "txtBATester";
            this.txtBATester.Size = new System.Drawing.Size(219, 20);
            this.txtBATester.TabIndex = 14;
            this.txtBATester.Tag = "<! <BA>!>";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Business Analyst:";
            // 
            // btnChangeDate
            // 
            this.btnChangeDate.Location = new System.Drawing.Point(357, 187);
            this.btnChangeDate.Name = "btnChangeDate";
            this.btnChangeDate.Size = new System.Drawing.Size(25, 23);
            this.btnChangeDate.TabIndex = 12;
            this.btnChangeDate.Text = "...";
            this.btnChangeDate.UseVisualStyleBackColor = true;
            this.btnChangeDate.Click += new System.EventHandler(this.btnChangeDate_Click);
            // 
            // txtChangeDate
            // 
            this.txtChangeDate.Location = new System.Drawing.Point(132, 187);
            this.txtChangeDate.Name = "txtChangeDate";
            this.txtChangeDate.Size = new System.Drawing.Size(219, 20);
            this.txtChangeDate.TabIndex = 11;
            this.txtChangeDate.Tag = "<! <CHANGEDATE>!>";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(-2, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Requested Change Date:";
            // 
            // txtPurpose
            // 
            this.txtPurpose.Location = new System.Drawing.Point(132, 135);
            this.txtPurpose.Name = "txtPurpose";
            this.txtPurpose.Size = new System.Drawing.Size(250, 20);
            this.txtPurpose.TabIndex = 9;
            this.txtPurpose.Tag = "<! <PURPOSE>!>";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Purpose:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(132, 109);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(250, 20);
            this.txtDescription.TabIndex = 8;
            this.txtDescription.Tag = "<! <DESCR>!>";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Description:";
            // 
            // chkWEB
            // 
            this.chkWEB.AutoSize = true;
            this.chkWEB.Location = new System.Drawing.Point(270, 17);
            this.chkWEB.Name = "chkWEB";
            this.chkWEB.Size = new System.Drawing.Size(51, 17);
            this.chkWEB.TabIndex = 4;
            this.chkWEB.Tag = "<? <WEB>?>";
            this.chkWEB.Text = "WEB";
            this.chkWEB.UseVisualStyleBackColor = true;
            // 
            // chkPORTAL
            // 
            this.chkPORTAL.AutoSize = true;
            this.chkPORTAL.Location = new System.Drawing.Point(195, 17);
            this.chkPORTAL.Name = "chkPORTAL";
            this.chkPORTAL.Size = new System.Drawing.Size(69, 17);
            this.chkPORTAL.TabIndex = 3;
            this.chkPORTAL.Tag = "<? <PORTAL>?>";
            this.chkPORTAL.Text = "PORTAL";
            this.chkPORTAL.UseVisualStyleBackColor = true;
            // 
            // chkFIN
            // 
            this.chkFIN.AutoSize = true;
            this.chkFIN.Location = new System.Drawing.Point(146, 17);
            this.chkFIN.Name = "chkFIN";
            this.chkFIN.Size = new System.Drawing.Size(43, 17);
            this.chkFIN.TabIndex = 2;
            this.chkFIN.Tag = "<? <FIN>?>";
            this.chkFIN.Text = "FIN";
            this.chkFIN.UseVisualStyleBackColor = true;
            // 
            // chkCRM
            // 
            this.chkCRM.AutoSize = true;
            this.chkCRM.Location = new System.Drawing.Point(90, 17);
            this.chkCRM.Name = "chkCRM";
            this.chkCRM.Size = new System.Drawing.Size(50, 17);
            this.chkCRM.TabIndex = 1;
            this.chkCRM.Tag = "<? <CRM>?>";
            this.chkCRM.Text = "CRM";
            this.chkCRM.UseVisualStyleBackColor = true;
            // 
            // chkCS
            // 
            this.chkCS.AutoSize = true;
            this.chkCS.Location = new System.Drawing.Point(44, 17);
            this.chkCS.Name = "chkCS";
            this.chkCS.Size = new System.Drawing.Size(40, 17);
            this.chkCS.TabIndex = 0;
            this.chkCS.Tag = "<? <CS>?>";
            this.chkCS.Text = "CS";
            this.chkCS.UseVisualStyleBackColor = true;
            // 
            // txtTicketNumber
            // 
            this.txtTicketNumber.Location = new System.Drawing.Point(132, 79);
            this.txtTicketNumber.Name = "txtTicketNumber";
            this.txtTicketNumber.Size = new System.Drawing.Size(219, 20);
            this.txtTicketNumber.TabIndex = 7;
            this.txtTicketNumber.Tag = "<! <TICKET#>!>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ticket Number:";
            // 
            // btnChangeRequest
            // 
            this.btnChangeRequest.Location = new System.Drawing.Point(357, 53);
            this.btnChangeRequest.Name = "btnChangeRequest";
            this.btnChangeRequest.Size = new System.Drawing.Size(25, 23);
            this.btnChangeRequest.TabIndex = 6;
            this.btnChangeRequest.Text = "...";
            this.btnChangeRequest.UseVisualStyleBackColor = true;
            this.btnChangeRequest.Click += new System.EventHandler(this.btnChangeRequest_Click);
            // 
            // txtChangeRequestNumber
            // 
            this.txtChangeRequestNumber.Location = new System.Drawing.Point(132, 53);
            this.txtChangeRequestNumber.Name = "txtChangeRequestNumber";
            this.txtChangeRequestNumber.Size = new System.Drawing.Size(219, 20);
            this.txtChangeRequestNumber.TabIndex = 5;
            this.txtChangeRequestNumber.Tag = "<! <CR#>!>";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Change Request #:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.textBox3);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.textBox2);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.textBox1);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(454, 355);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Security";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(49, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(332, 26);
            this.label12.TabIndex = 8;
            this.label12.Text = "These fields are text-only, and do not suppport multiple lines. \r\nWrite your cont" +
    "ent in paragraph form and format properly in word later\r\n";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(116, 217);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(252, 61);
            this.textBox3.TabIndex = 7;
            this.textBox3.Tag = "<! <USERACCESS>!>";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(73, 217);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Users:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(116, 150);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(252, 61);
            this.textBox2.TabIndex = 5;
            this.textBox2.Tag = "<! <NAVIGATION>!>";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(49, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Navigation:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(116, 83);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(252, 61);
            this.textBox1.TabIndex = 3;
            this.textBox1.Tag = "<! <SECURITYNEEDS>!>";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Security Needs:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox2);
            this.tabPage5.Controls.Add(this.txtBuildRequired);
            this.tabPage5.Controls.Add(this.groupBox1);
            this.tabPage5.Controls.Add(this.chkBuildRequired);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.txtProjectName);
            this.tabPage5.Controls.Add(this.label15);
            this.tabPage5.Controls.Add(this.cmboDestination);
            this.tabPage5.Controls.Add(this.label14);
            this.tabPage5.Controls.Add(this.cmboSource);
            this.tabPage5.Controls.Add(this.label13);
            this.tabPage5.Controls.Add(this.cmboPillar);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(454, 355);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Migration";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.optExecuteBuild);
            this.groupBox2.Controls.Add(this.optExecuteNow);
            this.groupBox2.Controls.Add(this.optBuildScript);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(33, 208);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(362, 71);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Build Options";
            // 
            // optExecuteBuild
            // 
            this.optExecuteBuild.AutoSize = true;
            this.optExecuteBuild.Location = new System.Drawing.Point(224, 31);
            this.optExecuteBuild.Name = "optExecuteBuild";
            this.optExecuteBuild.Size = new System.Drawing.Size(138, 17);
            this.optExecuteBuild.TabIndex = 2;
            this.optExecuteBuild.TabStop = true;
            this.optExecuteBuild.Tag = "<! <BUILDEXEC>!>";
            this.optExecuteBuild.Text = "Execute and build script";
            this.optExecuteBuild.UseVisualStyleBackColor = true;
            // 
            // optExecuteNow
            // 
            this.optExecuteNow.AutoSize = true;
            this.optExecuteNow.Location = new System.Drawing.Point(107, 31);
            this.optExecuteNow.Name = "optExecuteNow";
            this.optExecuteNow.Size = new System.Drawing.Size(111, 17);
            this.optExecuteNow.TabIndex = 1;
            this.optExecuteNow.TabStop = true;
            this.optExecuteNow.Tag = "<! <EXECUTESQL>!>";
            this.optExecuteNow.Text = "Execute SQL now";
            this.optExecuteNow.UseVisualStyleBackColor = true;
            // 
            // optBuildScript
            // 
            this.optBuildScript.AutoSize = true;
            this.optBuildScript.Location = new System.Drawing.Point(9, 31);
            this.optBuildScript.Name = "optBuildScript";
            this.optBuildScript.Size = new System.Drawing.Size(92, 17);
            this.optBuildScript.TabIndex = 0;
            this.optBuildScript.TabStop = true;
            this.optBuildScript.Tag = "<! <BUILDSCRIPT>!>";
            this.optBuildScript.Text = "Build script file";
            this.optBuildScript.UseVisualStyleBackColor = true;
            // 
            // txtBuildRequired
            // 
            this.txtBuildRequired.Location = new System.Drawing.Point(133, 105);
            this.txtBuildRequired.Name = "txtBuildRequired";
            this.txtBuildRequired.Size = new System.Drawing.Size(121, 20);
            this.txtBuildRequired.TabIndex = 10;
            this.txtBuildRequired.Tag = "<! <BUILDREQ>!>";
            this.txtBuildRequired.Text = "No Build Required";
            this.txtBuildRequired.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkCreateTrigger);
            this.groupBox1.Controls.Add(this.chkAlterTable);
            this.groupBox1.Controls.Add(this.chkCreateViews);
            this.groupBox1.Controls.Add(this.chkCreateIndex);
            this.groupBox1.Controls.Add(this.chkCreateTable);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(32, 131);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(362, 71);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Build Options";
            // 
            // chkCreateTrigger
            // 
            this.chkCreateTrigger.AutoSize = true;
            this.chkCreateTrigger.Location = new System.Drawing.Point(208, 19);
            this.chkCreateTrigger.Name = "chkCreateTrigger";
            this.chkCreateTrigger.Size = new System.Drawing.Size(93, 17);
            this.chkCreateTrigger.TabIndex = 13;
            this.chkCreateTrigger.Tag = "<! <CREATETRIGGER>!>";
            this.chkCreateTrigger.Text = "Create Trigger";
            this.chkCreateTrigger.UseVisualStyleBackColor = true;
            // 
            // chkAlterTable
            // 
            this.chkAlterTable.AutoSize = true;
            this.chkAlterTable.Location = new System.Drawing.Point(107, 42);
            this.chkAlterTable.Name = "chkAlterTable";
            this.chkAlterTable.Size = new System.Drawing.Size(77, 17);
            this.chkAlterTable.TabIndex = 12;
            this.chkAlterTable.Tag = "<! <ALTERTABLE>!>";
            this.chkAlterTable.Text = "Alter Table";
            this.chkAlterTable.UseVisualStyleBackColor = true;
            this.chkAlterTable.CheckedChanged += new System.EventHandler(this.chkAlterTable_CheckedChanged);
            // 
            // chkCreateViews
            // 
            this.chkCreateViews.AutoSize = true;
            this.chkCreateViews.Location = new System.Drawing.Point(107, 19);
            this.chkCreateViews.Name = "chkCreateViews";
            this.chkCreateViews.Size = new System.Drawing.Size(88, 17);
            this.chkCreateViews.TabIndex = 11;
            this.chkCreateViews.Tag = "<! <CREATEVIEWS>!>";
            this.chkCreateViews.Text = "Create Views";
            this.chkCreateViews.UseVisualStyleBackColor = true;
            // 
            // chkCreateIndex
            // 
            this.chkCreateIndex.AutoSize = true;
            this.chkCreateIndex.Location = new System.Drawing.Point(6, 42);
            this.chkCreateIndex.Name = "chkCreateIndex";
            this.chkCreateIndex.Size = new System.Drawing.Size(86, 17);
            this.chkCreateIndex.TabIndex = 10;
            this.chkCreateIndex.Tag = "<! <CREATEINDEX>!>";
            this.chkCreateIndex.Text = "Create Index";
            this.chkCreateIndex.UseVisualStyleBackColor = true;
            // 
            // chkCreateTable
            // 
            this.chkCreateTable.AutoSize = true;
            this.chkCreateTable.Location = new System.Drawing.Point(6, 19);
            this.chkCreateTable.Name = "chkCreateTable";
            this.chkCreateTable.Size = new System.Drawing.Size(92, 17);
            this.chkCreateTable.TabIndex = 9;
            this.chkCreateTable.Tag = "<! <CREATETABLE>!>";
            this.chkCreateTable.Text = "Create Tables";
            this.chkCreateTable.UseVisualStyleBackColor = true;
            this.chkCreateTable.CheckedChanged += new System.EventHandler(this.chkCreateTable_CheckedChanged);
            // 
            // chkBuildRequired
            // 
            this.chkBuildRequired.AutoSize = true;
            this.chkBuildRequired.Location = new System.Drawing.Point(32, 107);
            this.chkBuildRequired.Name = "chkBuildRequired";
            this.chkBuildRequired.Size = new System.Drawing.Size(95, 17);
            this.chkBuildRequired.TabIndex = 8;
            this.chkBuildRequired.Tag = "[NONE]";
            this.chkBuildRequired.Text = "Build Required";
            this.chkBuildRequired.UseVisualStyleBackColor = true;
            this.chkBuildRequired.CheckedChanged += new System.EventHandler(this.chkBuildRequired_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(30, 84);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 7;
            this.label16.Text = "Project:";
            // 
            // txtProjectName
            // 
            this.txtProjectName.Location = new System.Drawing.Point(79, 81);
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(121, 20);
            this.txtProjectName.TabIndex = 6;
            this.txtProjectName.Tag = "<! <PROJECTNAME>!>";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(204, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Destination:";
            // 
            // cmboDestination
            // 
            this.cmboDestination.FormattingEnabled = true;
            this.cmboDestination.Items.AddRange(new object[] {
            "CS",
            "CRM",
            "PORTAL",
            "FIN"});
            this.cmboDestination.Location = new System.Drawing.Point(273, 54);
            this.cmboDestination.Name = "cmboDestination";
            this.cmboDestination.Size = new System.Drawing.Size(121, 21);
            this.cmboDestination.TabIndex = 4;
            this.cmboDestination.Tag = "<! <DEST>!>";
            this.cmboDestination.Text = "Select One";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(29, 57);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Source:";
            // 
            // cmboSource
            // 
            this.cmboSource.FormattingEnabled = true;
            this.cmboSource.Items.AddRange(new object[] {
            "CS",
            "CRM",
            "PORTAL",
            "FIN"});
            this.cmboSource.Location = new System.Drawing.Point(79, 54);
            this.cmboSource.Name = "cmboSource";
            this.cmboSource.Size = new System.Drawing.Size(121, 21);
            this.cmboSource.TabIndex = 2;
            this.cmboSource.Tag = "<! <SOURCE>!>";
            this.cmboSource.Text = "Select One";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(133, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Pillar:";
            // 
            // cmboPillar
            // 
            this.cmboPillar.FormattingEnabled = true;
            this.cmboPillar.Items.AddRange(new object[] {
            "CS",
            "CRM",
            "PORTAL",
            "FIN"});
            this.cmboPillar.Location = new System.Drawing.Point(171, 25);
            this.cmboPillar.Name = "cmboPillar";
            this.cmboPillar.Size = new System.Drawing.Size(121, 21);
            this.cmboPillar.TabIndex = 0;
            this.cmboPillar.Tag = "[NONE]";
            this.cmboPillar.Text = "Select One";
            this.cmboPillar.SelectedIndexChanged += new System.EventHandler(this.cmboPillar_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtAlterAny);
            this.tabPage2.Controls.Add(this.chkChangesToDefault);
            this.tabPage2.Controls.Add(this.checkBox5);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(454, 355);
            this.tabPage2.TabIndex = 5;
            this.tabPage2.Text = "Build Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtAlterAny
            // 
            this.txtAlterAny.Enabled = false;
            this.txtAlterAny.Location = new System.Drawing.Point(369, 287);
            this.txtAlterAny.Name = "txtAlterAny";
            this.txtAlterAny.Size = new System.Drawing.Size(79, 20);
            this.txtAlterAny.TabIndex = 4;
            this.txtAlterAny.Tag = "<! <ALTERANY>!>";
            this.txtAlterAny.Text = "ALL";
            // 
            // chkChangesToDefault
            // 
            this.chkChangesToDefault.AutoSize = true;
            this.chkChangesToDefault.Location = new System.Drawing.Point(35, 14);
            this.chkChangesToDefault.Name = "chkChangesToDefault";
            this.chkChangesToDefault.Size = new System.Drawing.Size(208, 17);
            this.chkChangesToDefault.TabIndex = 8;
            this.chkChangesToDefault.Tag = "<! <CHANGENEEDED>!>";
            this.chkChangesToDefault.Text = "Changes To Default Settings Required";
            this.chkChangesToDefault.UseVisualStyleBackColor = true;
            this.chkChangesToDefault.CheckedChanged += new System.EventHandler(this.chkChangesToDefault_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Enabled = false;
            this.checkBox5.Location = new System.Drawing.Point(147, 324);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(146, 17);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Tag = "<! <ALTERNOCHANGE>!>";
            this.checkBox5.Text = "Alter Even If No Changes";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.chkAlterDeletes);
            this.groupBox9.Controls.Add(this.chkAlterRenames);
            this.groupBox9.Controls.Add(this.chkAlterChanges);
            this.groupBox9.Controls.Add(this.chkAlterAdd);
            this.groupBox9.Enabled = false;
            this.groupBox9.Location = new System.Drawing.Point(82, 268);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(281, 50);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Alter Any";
            // 
            // chkAlterDeletes
            // 
            this.chkAlterDeletes.AutoSize = true;
            this.chkAlterDeletes.Checked = true;
            this.chkAlterDeletes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAlterDeletes.Location = new System.Drawing.Point(212, 19);
            this.chkAlterDeletes.Name = "chkAlterDeletes";
            this.chkAlterDeletes.Size = new System.Drawing.Size(62, 17);
            this.chkAlterDeletes.TabIndex = 3;
            this.chkAlterDeletes.Tag = "DELETE";
            this.chkAlterDeletes.Text = "Deletes";
            this.chkAlterDeletes.UseVisualStyleBackColor = true;
            // 
            // chkAlterRenames
            // 
            this.chkAlterRenames.AutoSize = true;
            this.chkAlterRenames.Checked = true;
            this.chkAlterRenames.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAlterRenames.Location = new System.Drawing.Point(140, 19);
            this.chkAlterRenames.Name = "chkAlterRenames";
            this.chkAlterRenames.Size = new System.Drawing.Size(71, 17);
            this.chkAlterRenames.TabIndex = 2;
            this.chkAlterRenames.Tag = "RENAME";
            this.chkAlterRenames.Text = "Renames";
            this.chkAlterRenames.UseVisualStyleBackColor = true;
            // 
            // chkAlterChanges
            // 
            this.chkAlterChanges.AutoSize = true;
            this.chkAlterChanges.Checked = true;
            this.chkAlterChanges.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAlterChanges.Location = new System.Drawing.Point(74, 19);
            this.chkAlterChanges.Name = "chkAlterChanges";
            this.chkAlterChanges.Size = new System.Drawing.Size(68, 17);
            this.chkAlterChanges.TabIndex = 1;
            this.chkAlterChanges.Tag = "CHANGE";
            this.chkAlterChanges.Text = "Changes";
            this.chkAlterChanges.UseVisualStyleBackColor = true;
            // 
            // chkAlterAdd
            // 
            this.chkAlterAdd.AutoSize = true;
            this.chkAlterAdd.Checked = true;
            this.chkAlterAdd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAlterAdd.Location = new System.Drawing.Point(10, 19);
            this.chkAlterAdd.Name = "chkAlterAdd";
            this.chkAlterAdd.Size = new System.Drawing.Size(50, 17);
            this.chkAlterAdd.TabIndex = 0;
            this.chkAlterAdd.Tag = "ADD";
            this.chkAlterAdd.Text = "Adds";
            this.chkAlterAdd.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.radioButton11);
            this.groupBox8.Controls.Add(this.radioButton12);
            this.groupBox8.Enabled = false;
            this.groupBox8.Location = new System.Drawing.Point(219, 191);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(180, 71);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Alter Table Options";
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Checked = true;
            this.radioButton11.Location = new System.Drawing.Point(15, 42);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(134, 17);
            this.radioButton11.TabIndex = 1;
            this.radioButton11.TabStop = true;
            this.radioButton11.Tag = "<! <ALTERBYRENAME>!>";
            this.radioButton11.Text = "Alter By Table Rename";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Location = new System.Drawing.Point(15, 19);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(88, 17);
            this.radioButton12.TabIndex = 0;
            this.radioButton12.Tag = "[NONE]";
            this.radioButton12.Text = "Alter In Place";
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.radioButton9);
            this.groupBox7.Controls.Add(this.radioButton10);
            this.groupBox7.Enabled = false;
            this.groupBox7.Location = new System.Drawing.Point(219, 114);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(180, 71);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Change Column Length Options";
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Checked = true;
            this.radioButton9.Location = new System.Drawing.Point(15, 42);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(130, 17);
            this.radioButton9.TabIndex = 1;
            this.radioButton9.TabStop = true;
            this.radioButton9.Tag = "<! <SKIPIFSHORT>!>";
            this.radioButton9.Text = "Skip If Field Too Short";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Location = new System.Drawing.Point(15, 19);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(152, 17);
            this.radioButton10.TabIndex = 0;
            this.radioButton10.Tag = "[NONE]";
            this.radioButton10.Text = "Truncate If Field Too Short";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radioButton7);
            this.groupBox6.Controls.Add(this.radioButton8);
            this.groupBox6.Enabled = false;
            this.groupBox6.Location = new System.Drawing.Point(219, 37);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(180, 71);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Drop Column Options";
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Checked = true;
            this.radioButton7.Location = new System.Drawing.Point(15, 42);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(158, 17);
            this.radioButton7.TabIndex = 1;
            this.radioButton7.TabStop = true;
            this.radioButton7.Tag = "<! <SKIPIFPRESENT>!>";
            this.radioButton7.Text = "Skip Record If Data Present";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(15, 19);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(160, 17);
            this.radioButton8.TabIndex = 0;
            this.radioButton8.Tag = "[NONE]";
            this.radioButton8.Text = "Drop Column If Data Present";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioButton5);
            this.groupBox5.Controls.Add(this.radioButton6);
            this.groupBox5.Enabled = false;
            this.groupBox5.Location = new System.Drawing.Point(35, 191);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(168, 71);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Index Creation Options";
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Checked = true;
            this.radioButton5.Location = new System.Drawing.Point(15, 42);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(150, 17);
            this.radioButton5.TabIndex = 1;
            this.radioButton5.TabStop = true;
            this.radioButton5.Tag = "<! <REMAKEINDEX>!>";
            this.radioButton5.Text = "Recreate Index If Modified";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(15, 19);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(137, 17);
            this.radioButton6.TabIndex = 0;
            this.radioButton6.Tag = "[NONE]";
            this.radioButton6.Text = "Recreate Index If Exists";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioButton3);
            this.groupBox4.Controls.Add(this.radioButton4);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(35, 114);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(168, 71);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "View Creation Options";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(15, 42);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(111, 17);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.Tag = "[NONE]";
            this.radioButton3.Text = "Skip View If Exists";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Checked = true;
            this.radioButton4.Location = new System.Drawing.Point(15, 19);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(134, 17);
            this.radioButton4.TabIndex = 0;
            this.radioButton4.TabStop = true;
            this.radioButton4.Tag = "<! <REMAKEVIEW>!>";
            this.radioButton4.Text = "Recreate View If Exists";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new System.Drawing.Point(35, 37);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(168, 71);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Table Creation Options";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(15, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(115, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Tag = "<! <SKIPTABLES>!>";
            this.radioButton2.Text = "Skip Table If Exists";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(15, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(138, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.Tag = "[NONE]";
            this.radioButton1.Text = "Recreate Table If Exists";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(362, 413);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(486, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.settingsToolStripMenuItem.Text = "&Settings...";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(132, 265);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(219, 20);
            this.textBox4.TabIndex = 23;
            this.textBox4.Tag = "<! <TESTER>!>";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(86, 268);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Tester:";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 448);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "DevPac Assistant";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabs.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnChangeDate;
        private System.Windows.Forms.TextBox txtChangeDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPurpose;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkWEB;
        private System.Windows.Forms.CheckBox chkPORTAL;
        private System.Windows.Forms.CheckBox chkFIN;
        private System.Windows.Forms.CheckBox chkCRM;
        private System.Windows.Forms.CheckBox chkCS;
        private System.Windows.Forms.TextBox txtTicketNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnChangeRequest;
        private System.Windows.Forms.TextBox txtChangeRequestNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkSecurity;
        private System.Windows.Forms.TextBox txtBATester;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TextBox txtEnvironment;
        private System.Windows.Forms.TextBox txtRequestor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDeveloper;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmboDestination;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmboSource;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmboPillar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkCreateTrigger;
        private System.Windows.Forms.CheckBox chkAlterTable;
        private System.Windows.Forms.CheckBox chkCreateViews;
        private System.Windows.Forms.CheckBox chkCreateIndex;
        private System.Windows.Forms.CheckBox chkCreateTable;
        private System.Windows.Forms.CheckBox chkBuildRequired;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtProjectName;
        private System.Windows.Forms.TextBox txtBuildRequired;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton optExecuteBuild;
        private System.Windows.Forms.RadioButton optExecuteNow;
        private System.Windows.Forms.RadioButton optBuildScript;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox chkAlterDeletes;
        private System.Windows.Forms.CheckBox chkAlterRenames;
        private System.Windows.Forms.CheckBox chkAlterChanges;
        private System.Windows.Forms.CheckBox chkAlterAdd;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox chkChangesToDefault;
        private System.Windows.Forms.TextBox txtAlterAny;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label17;

    }
}

