﻿namespace DevPac_Creator
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtDevName = new System.Windows.Forms.TextBox();
            this.txtDevInitials = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCRFolder = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCRFolder = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Developer Name:";
            // 
            // txtDevName
            // 
            this.txtDevName.Location = new System.Drawing.Point(107, 12);
            this.txtDevName.Name = "txtDevName";
            this.txtDevName.Size = new System.Drawing.Size(166, 20);
            this.txtDevName.TabIndex = 1;
            // 
            // txtDevInitials
            // 
            this.txtDevInitials.Location = new System.Drawing.Point(107, 39);
            this.txtDevInitials.Name = "txtDevInitials";
            this.txtDevInitials.Size = new System.Drawing.Size(80, 20);
            this.txtDevInitials.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Initials:";
            // 
            // txtCRFolder
            // 
            this.txtCRFolder.Location = new System.Drawing.Point(107, 65);
            this.txtCRFolder.Name = "txtCRFolder";
            this.txtCRFolder.Size = new System.Drawing.Size(166, 20);
            this.txtCRFolder.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "CR Folder:";
            // 
            // btnCRFolder
            // 
            this.btnCRFolder.Location = new System.Drawing.Point(279, 65);
            this.btnCRFolder.Name = "btnCRFolder";
            this.btnCRFolder.Size = new System.Drawing.Size(27, 23);
            this.btnCRFolder.TabIndex = 6;
            this.btnCRFolder.Text = "...";
            this.btnCRFolder.UseVisualStyleBackColor = true;
            this.btnCRFolder.Click += new System.EventHandler(this.btnCRFolder_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(137, 92);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save Settings";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 127);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCRFolder);
            this.Controls.Add(this.txtCRFolder);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDevInitials);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDevName);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDevName;
        private System.Windows.Forms.TextBox txtDevInitials;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCRFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCRFolder;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
    }
}