﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;

namespace DevPac_Creator
{
    public partial class Main : Form
    {
        CheckBox[] environments = new CheckBox[5];
        CheckBox[] alterAny = new CheckBox[4];

        Hashtable values = new Hashtable();
//        ZipStorer zipStorer = ZipStorer.Open(Application.StartupPath + "\template.docx");
        public Main()
        {
            InitializeComponent();

            // Set up array for Environment checkboxes on Tab 1 (General Information)
            environments[0] = chkCS;
            environments[1] = chkCRM;
            environments[2] = chkFIN;
            environments[3] = chkPORTAL;
            environments[4] = chkWEB;

            environments[0].CheckedChanged += Environment_CheckedChanged;
            environments[1].CheckedChanged += Environment_CheckedChanged;
            environments[2].CheckedChanged += Environment_CheckedChanged;
            environments[3].CheckedChanged += Environment_CheckedChanged;
            environments[4].CheckedChanged += Environment_CheckedChanged;

            alterAny[0] = chkAlterAdd;
            alterAny[1] = chkAlterChanges;
            alterAny[2] = chkAlterRenames;
            alterAny[3] = chkAlterDeletes;

            alterAny[0].CheckedChanged += AlterAny_CheckedChanged;
            alterAny[1].CheckedChanged += AlterAny_CheckedChanged;
            alterAny[2].CheckedChanged += AlterAny_CheckedChanged;
            alterAny[3].CheckedChanged += AlterAny_CheckedChanged;

        }

        void Environment_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = ((CheckBox)sender);
            if (cb.Checked)
            {
                txtEnvironment.Text += " " + (String)cb.Text;
                txtEnvironment.Text = txtEnvironment.Text.Trim();
            }
            else
            {
                txtEnvironment.Text = txtEnvironment.Text.Replace((String)cb.Text, "").Trim();
            }
        }

        void AlterAny_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = ((CheckBox)sender);
            if (cb.Checked)
            {
                txtAlterAny.Text += " " + (String)cb.Tag;
                txtAlterAny.Text = txtAlterAny.Text.Trim();
            }
            else
            {
                txtAlterAny.Text = txtAlterAny.Text.Replace((String)cb.Tag, "").Trim();
            }

            if (txtAlterAny.Text.Length >= 24)
            {
                txtAlterAny.Text = "ALL";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            // remove the security tab until we know it's needed
            tabs.TabPages.Remove(tabPage3);
            tabs.TabPages.Remove(tabPage2);


            if (!checkSettings())
            {
                MessageBox.Show("Some required settings have not been set, please do so before using the application.");
                Settings settings = new Settings();
                settings.ShowDialog();
            }

        }

        private bool checkSettings()
        {
            if (Properties.Settings.Default.CRFolder != "")
                if (Properties.Settings.Default.DeveloperInitials != "")
                    if (Properties.Settings.Default.DeveloperName != "")
                        return true;

            return false;

        }

        private void btnNext_Click(object sender, EventArgs e)
        {

            updateReplacementValues(tabs.TabPages[tabs.SelectedIndex]);
            if (btnNext.Text == "Finish")
            {
                StringBuilder sb = new StringBuilder();
                foreach (Object o in values.Keys)
                {
                    sb.AppendLine(o + " = " + values[o]);
                }
                Clipboard.SetText(sb.ToString());

                if (tabs.TabPages.Contains(tabPage2) == false)
                {
                    updateReplacementValues(tabPage2);
                }
                if (tabs.TabPages.Contains(tabPage3) == false)
                {
                    updateReplacementValues(tabPage3);
                }


                CreateFromTemplate(values);
            }
            
            tabs.SelectedIndex++;
            if (tabs.SelectedIndex == tabs.TabCount - 1)
            {
                btnNext.Text = "Finish";
            }
            else
            {
                btnNext.Text = "Next";
            }

            

        }

        private void updateReplacementValues(TabPage tp)
        {
            foreach (Control c in tp.Controls)
            {
                if (c.GetType() == typeof(GroupBox))
                {
                    foreach (Control ct in c.Controls)
                    {
                        addItemToValues(ct);
                    }
                }
                else
                {
                    addItemToValues(c);
                }
            }
        }

        private void addItemToValues(Control c)
        {
            if (c.GetType() == typeof(TextBox))
            {
                // add to Hashtable here

                if (values.ContainsKey(c.Tag))
                {
                    values[c.Tag] = c.Text;
                }
                else
                {
                    values.Add(c.Tag, c.Text);
                }
            }

            if (c.GetType() == typeof(CheckBox) || c.GetType() == typeof(RadioButton))
            {
                // add to Hashtable here
                bool selected = (bool)c.GetType().GetProperty("Checked").GetValue(c);

                if (values.ContainsKey(c.Tag))
                {
                    
                    values[c.Tag] = selected ? "Yes" : "No";

                }
                else
                {
                    values.Add(c.Tag, selected ? "Yes" : "No");
                }
            }
            if (c.GetType() == typeof(ComboBox))
            {
                if (values.ContainsKey(c.Tag))
                {
                    values[c.Tag] = c.Text;
                }
                else
                {
                    values.Add(c.Tag, c.Text);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.ShowDialog(this);
        }

        private void chkSecurity_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSecurity.Checked)
            {
                tabs.TabPages.Insert(1, tabPage3);
            }
            else
            {
                tabs.TabPages.Remove(tabPage3);
            }
        }

        private void btnChangeDate_Click(object sender, EventArgs e)
        {
            txtChangeDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
        }

        private void btnChangeRequest_Click(object sender, EventArgs e)
        {
            // This generates a CR Folder for you, need to have Environments and Descr set up for it to work.

            // Make sure CR Folder has been set in Settings
            if (Properties.Settings.Default.CRFolder == "")
            {
                MessageBox.Show("Please set the Change Request folder in Settings.", "Error");
                return;
            }

            // Check for Requisites
            bool environmentSelected = false;
            StringBuilder sb = new StringBuilder();

            foreach (CheckBox cb in environments)
            {
                if (!environmentSelected)
                {
                    environmentSelected = cb.Checked;
                }
                if (cb.Checked)
                {
                    sb.Append(cb.Text.ToUpper() + " ");
                }
            }
            if (sb.Length > 0)
                sb.Remove(sb.Length - 1, 1);


            if (!environmentSelected)
            {
                MessageBox.Show("Please select your environment(s) first.", "Error");
                return;
            }

            if (txtDescription.Text.Trim() == "")
            {
                MessageBox.Show("Please enter a description first.", "Error");
                return;
            }

            String CR_Number = makeCRFolder(sb.ToString(), txtDescription.Text);
            txtChangeRequestNumber.Text = CR_Number;

        }

        private string makeCRFolder(string environments, string description)
        {
            String crfolder = Properties.Settings.Default.CRFolder;
            String[] folders = Directory.GetDirectories(crfolder);
            
            ArrayList monthMatch = new ArrayList();

            string year = DateTime.Now.ToString("yy");
            string month = DateTime.Now.ToString("MM");
            
            foreach (String s in folders)
            {

                if (s.Replace(crfolder + "\\", "").StartsWith(year + month)) 
                {
                    monthMatch.Add(s.Replace(crfolder + "\\", ""));
                }
            }

            int highestNumber = 0;

            foreach (String s in monthMatch)
            {
                int monthChangeNumber = Int32.Parse(s.Substring(4,4));

                highestNumber = monthChangeNumber > highestNumber ? monthChangeNumber : highestNumber;
            }

            String folderName = year + month + (highestNumber + 1).ToString("D4") + " [" + environments + "] - " + description;
            Directory.CreateDirectory(crfolder + "\\" + folderName);
            return year+month + (highestNumber + 1).ToString("D4");
        }

        private void cmboPillar_SelectedIndexChanged(object sender, EventArgs e)
        {
            String environmentOptions = "DBVU1J--DBVU1G--DBVU1P--DBVU1K--SBVU1J--SBVU1G--SBVU1P--SBVU1K--TBVU1J--TBVU1G--TBVU1P--TBVU1K--PBVU1J--PBVU1G--PBVU1P--PBVU1K";
            String[] options = environmentOptions.Split("--".ToCharArray());
            String postfix = ""; ;
            switch (cmboPillar.Text)
            {
                case "CS":
                    postfix = "J";
                    break;
                case "CRM":
                    postfix = "G";
                    break;
                case "PORTAL":
                    postfix = "P";
                    break;
                case "FIN":
                    postfix = "K";
                    break;
            }
            cmboSource.Items.Clear();
            cmboSource.Text = "Select One";
            
            cmboDestination.Items.Clear();
            cmboDestination.Text = "Select One";

            foreach (String s in options)
            {
                if (s.EndsWith(postfix))
                {
                    cmboSource.Items.Add(s);
                    cmboDestination.Items.Add(s);
                }
            }
        }

        private void chkBuildRequired_CheckedChanged(object sender, EventArgs e)
        {
            switch (chkBuildRequired.Checked)
            {
                case true:
                    groupBox1.Enabled = true;
                    groupBox2.Enabled = true;
                    txtBuildRequired.Text = "Build Required";
                    tabs.TabPages.Add(tabPage2);
                    break;
                case false:
                    groupBox1.Enabled = false;
                    groupBox2.Enabled = false;
                    tabs.TabPages.Remove(tabPage2);
                    foreach (Control c in groupBox1.Controls)
                    {
                        ((CheckBox)c).Checked = false;
                    }
                    foreach (Control c in groupBox2.Controls)
                    {
                        ((RadioButton)c).Checked = false;
                    }
                    txtBuildRequired.Text = "No Build Required";
                    break;
            }

            if (tabs.SelectedIndex == tabs.TabCount - 1)
            {
                btnNext.Text = "Finish";
            }
            else
            {
                btnNext.Text = "Next";
            }
        }

        private void chkCreateTable_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCreateTable.Checked)
            {
                chkCreateIndex.Checked = true;
                chkCreateIndex.Enabled = false;
            }
            else
            {
                chkCreateIndex.Enabled = true;
            }
        }

        private void chkAlterTable_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAlterTable.Checked)
            {
                chkCreateTrigger.Checked = true;
                chkCreateTrigger.Enabled = false;
                optExecuteNow.Checked = false;
                optBuildScript.Checked = true;
                optExecuteNow.Enabled = false;
            }
            else
            {
                chkCreateTrigger.Enabled = true;
                optExecuteNow.Enabled = true;
            }
        }

        private void chkChangesToDefault_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control c in tabPage2.Controls)
            {
                if (c != chkChangesToDefault)
                    c.Enabled = !c.Enabled;
            }

            
        }


        private String CreateFromTemplate(Hashtable values)
        {
            String newFile = Path.GetTempFileName();
            String xmlFile = Path.GetTempFileName();
            
            File.Copy("Template.docx", newFile,true);
            ZipStorer zip = ZipStorer.Open(newFile,FileAccess.ReadWrite);
            ZipStorer.ZipFileEntry fileEntry = new ZipStorer.ZipFileEntry();
            foreach (var dir in zip.ReadCentralDir())
            {
                if (Path.GetFileName(dir.FilenameInZip) == "document.xml")
                {
                    fileEntry = dir;
                    zip.ExtractFile(dir, xmlFile);
                }
            }
            StringBuilder sb = new StringBuilder(File.ReadAllText(xmlFile));
            foreach (String s in values.Keys)
            {
                if (s.StartsWith("<! <"))
                {
                    sb = sb.Replace(System.Web.HttpUtility.HtmlEncode(s), (String)values[s]);
                }
                if (s.StartsWith("<? <"))
                {
                    if (values[s] == "Yes")
                    {
                        String pillar = s.Replace("<? <", "").Replace(">?>", "");
                        int entry = 0;
                        switch (pillar)
                        {
                            case "CS":
                                entry = 0;
                                break;
                            case "CRM":
                                entry = 4;
                                break;
                            case "FIN":
                                entry = 3;
                                break;
                            case "PORTAL":
                                entry = 1;
                                break;
                            case "WEB":
                                entry = 2;
                                break;
                        }
                        int replaceOffset = 0;
                        for (int x = entry; x >= 0; x--)
                        {
                            replaceOffset = sb.ToString().IndexOf("<a:schemeClr val=\"", replaceOffset + 1);
                        }
                        sb.Replace("<a:schemeClr val=\"bg1\"/>", "<a:schemeClr val=\"tx1\"/>", replaceOffset, "<a:schemeClr val=\"bg1\"/>".Length);
                    }
                }

            }
            File.WriteAllText(xmlFile,sb.ToString());

            ZipStorer.RemoveEntries(ref zip, fileEntry);
            zip.Close();

            zip = ZipStorer.Open(newFile, FileAccess.ReadWrite);
            zip.AddFile(ZipStorer.Compression.Deflate, xmlFile, "word/document.xml", "DevPac Assistant");
            zip.Close();

            File.Delete(xmlFile);
            File.Copy(newFile, "Result.docx",true);
            File.Delete(newFile);
            return Application.StartupPath + "\\Result.docx";
        }

    }
}
