﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevPac_Creator
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void btnCRFolder_Click(object sender, EventArgs e)
        {
            DialogResult dr = folderBrowser.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                txtCRFolder.Text = folderBrowser.SelectedPath;

            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //S:\ITS\Information Systems\Release Tracking\Change Requests
            Properties.Settings.Default.CRFolder = txtCRFolder.Text;
            Properties.Settings.Default.DeveloperName = txtDevName.Text;
            Properties.Settings.Default.DeveloperInitials = txtDevInitials.Text;
            this.Close();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            txtCRFolder.Text = Properties.Settings.Default.CRFolder;
            txtDevName.Text = Properties.Settings.Default.DeveloperName;
            txtDevInitials.Text = Properties.Settings.Default.DeveloperInitials;

            
        }
    }
}
